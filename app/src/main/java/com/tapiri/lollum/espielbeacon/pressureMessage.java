package com.tapiri.lollum.espielbeacon;

import android.util.Log;

import com.google.android.gms.nearby.messages.Message;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.google.android.gms.nearby.messages.Message;
import com.google.gson.Gson;

public class pressureMessage {
    private static final Gson gson = new Gson();

    private String mInstanceId;
    private String mMessageBody = "vuoto";
    private Double mPressure = 0.0;
    private int mType = 0; //0 LPS stazionario, 1 LPS movimento, 2 LPS troppo presto,3 GPS, ,4 BEACON
    private String mDatePressure;
    private String mDatePublication;


    public static Message newNearbyMessage(String instanceId) {
        pressureMessage pressureMessage = new pressureMessage(instanceId);
        return new Message(gson.toJson(pressureMessage).getBytes(Charset.forName("UTF-8")));
    }
    public static Message newNearbyMessage(String instanceId, int type ,Double pressure, String pressureDate) {
        pressureMessage pressureMessage = new pressureMessage(instanceId, type ,pressure, pressureDate);
        return new Message(gson.toJson(pressureMessage).getBytes(Charset.forName("UTF-8")));
    }
    public static pressureMessage fromNearbyMessage(Message message) {
        String nearbyMessageString = new String(message.getContent()).trim();
        return gson.fromJson(
                (new String(nearbyMessageString.getBytes(Charset.forName("UTF-8")))),
                pressureMessage.class);
    }

    private pressureMessage(String instanceId, int type ,Double pressure, String pressureData) {
        SimpleDateFormat timeFormatMessage = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);
        this.mInstanceId = instanceId;
        this.mPressure = pressure;
        this.mDatePublication = timeFormatMessage.format(Calendar.getInstance().getTime());
        this.mDatePressure = pressureData;
        this.mType = type;
        this.mMessageBody = mInstanceId+"|"+mType+"|"+mPressure+"|"+mDatePressure+"|"+mDatePublication;
    }
    private pressureMessage(String instanceId) {
        SimpleDateFormat timeFormatMessage = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);
        this.mInstanceId = instanceId;
        this.mPressure = 0.0;
        this.mDatePublication = timeFormatMessage.format(Calendar.getInstance().getTime());
        this.mDatePressure = timeFormatMessage.format(Calendar.getInstance().getTime());
        this.mType = 0;
        this.mMessageBody = "ACK";
    }
    protected String getMessageBody() {
        SimpleDateFormat timeFormatMessage = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);
        String typeString;
        Date datePressureDate;
        Date datePubblicationDate;

        switch(mType){
            case 0:
                typeString = "LPS-STAY";
                break;
            case 1:
                typeString = "LPS-MOVE";
                break;
            case 2:
                typeString = "LPS-SOON";
                break;
            case 3:
                typeString = "GPS";
                break;
            case 4:
                typeString = "BEACON";
                break;
            default:
                typeString = "UNKNOW";
                break;
        }

        try{
            datePressureDate = timeFormatMessage.parse(mDatePressure);
            datePubblicationDate = timeFormatMessage.parse(mDatePublication);
        }catch (Exception e){
            Log.e("ParseException ", e.toString());
            return mInstanceId+"-"+mPressure+"-"+mType;
        }
        return mInstanceId+"|"+mPressure+"|"+typeString+"|"+datePressureDate+"|"+datePubblicationDate;
    }

    protected  Date getDatePressure(){
        SimpleDateFormat timeFormatMessage = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);
        Date datePressureDate;
        try{
            datePressureDate = timeFormatMessage.parse(mDatePressure);
        }catch (Exception e){
            Log.e("ParseException ", e.toString());
            return null;
        }
        return datePressureDate;
    }
    protected  Date getDatePubblication(){
        SimpleDateFormat timeFormatMessage = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);
        Date datePubblicationDate;
        try{
            datePubblicationDate = timeFormatMessage.parse(mDatePublication);
        }catch (Exception e){
            Log.e("ParseException ", e.toString());
            return null;
        }
        return datePubblicationDate;
    }
    protected double getPressure(){
        return mPressure;
    }
    protected int getType(){
        return mType;
    }
}
