/**
 * Copyright 2015 Google Inc. All Rights Reserved.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.tapiri.lollum.espielbeacon;

import android.content.Context;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.messages.Message;
import com.google.android.gms.nearby.messages.MessageListener;
import com.google.android.gms.nearby.messages.NearbyMessagesStatusCodes;
import com.google.android.gms.nearby.messages.PublishCallback;
import com.google.android.gms.nearby.messages.PublishOptions;
import com.google.android.gms.nearby.messages.Strategy;
import com.google.android.gms.nearby.messages.SubscribeCallback;
import com.google.android.gms.nearby.messages.SubscribeOptions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = "MainFragment";
    private static final Strategy PUB_SUB_STRATEGY = new Strategy.Builder().setTtlSeconds(Constant.TTL_IN_SECONDS).build();
    private ProgressBar mSubscriptionProgressBar;
    private ImageButton mSubscriptionImageButton;
    private ProgressBar mPublicationProgressBar;
    private ImageButton mPublicationImageButton;
    private TextView mDateText;
    private Button mButtonRefresh;
    private RadioGroup mTypeSample;
    private int type;
    private EditText mPressureValue;
    private ArrayAdapter<String> mNearbyDevicesArrayAdapter;
    private final ArrayList<String> mNearbyDevicesArrayList = new ArrayList<>();
    private GoogleApiClient mGoogleApiClient;
    private Message mDeviceInfoMessage;
    private MessageListener mMessageListener;
    private boolean mResolvingNearbyPermissionError = false;
    private Date chooseDate;
    private SimpleDateFormat timeFormatText = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss", Locale.ITALY);

    public MainFragment() {
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Use a retained fragment to avoid re-publishing or re-subscribing upon orientation
        // changes.
        setRetainInstance(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        mSubscriptionProgressBar = (ProgressBar) view.findViewById(
                R.id.subscription_progress_bar);
        mSubscriptionImageButton = (ImageButton) view.findViewById(R.id.subscription_image_button);
        mSubscriptionImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i(TAG, "1");
                String subscriptionTask = getPubSubTask(Constant.KEY_SUBSCRIPTION_TASK);
                if (TextUtils.equals(subscriptionTask, Constant.TASK_NONE) ||
                        TextUtils.equals(subscriptionTask, Constant.TASK_UNSUBSCRIBE)) {
                    Log.i(TAG, "2");
                    updateSharedPreference(Constant.KEY_SUBSCRIPTION_TASK,
                            Constant.TASK_SUBSCRIBE);
                    Log.i(TAG, "3");
                } else {
                    Log.i(TAG, "4");
                    updateSharedPreference(Constant.KEY_SUBSCRIPTION_TASK,
                            Constant.TASK_UNSUBSCRIBE);
                    Log.i(TAG, "5");
                }
            }
        });
        Log.i(TAG, "40");
        mPublicationProgressBar = (ProgressBar) view.findViewById(R.id.publication_progress_bar);
        mPublicationImageButton = (ImageButton) view.findViewById(R.id.publication_image_button);
        mPublicationImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String publicationTask = getPubSubTask(Constant.KEY_PUBLICATION_TASK);
                if (TextUtils.equals(publicationTask, Constant.TASK_NONE) ||
                        TextUtils.equals(publicationTask, Constant.TASK_UNPUBLISH)) {
                    updateSharedPreference(Constant.KEY_PUBLICATION_TASK, Constant.TASK_PUBLISH);
                } else {
                    updateSharedPreference(Constant.KEY_PUBLICATION_TASK,
                            Constant.TASK_UNPUBLISH);
                }
            }
        });
        Log.i(TAG, "30");
        final ListView nearbyDevicesListView = (ListView) view.findViewById(
                R.id.nearby_devices_list_view);
        Log.i(TAG, "31");
        mNearbyDevicesArrayAdapter = new ArrayAdapter<>(getActivity().getApplicationContext(),
                android.R.layout.simple_list_item_1,
                mNearbyDevicesArrayList);
        Log.i(TAG, "32");
        nearbyDevicesListView.setAdapter(mNearbyDevicesArrayAdapter);
        Log.i(TAG, "33");
        mMessageListener = new MessageListener() {
            @Override
            public void onFound(final Message message) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mNearbyDevicesArrayAdapter.add(
                                pressureMessage.fromNearbyMessage(message).getMessageBody());
                    }
                });
            }

            @Override
            public void onLost(final Message message) {
                // Called when a message is no longer detectable nearby.
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mNearbyDevicesArrayAdapter.remove(
                                pressureMessage.fromNearbyMessage(message).getMessageBody());
                    }
                });
            }
        };
        Log.i(TAG, "34");
        mPressureValue = (EditText)view.findViewById(R.id.editText);
        // Upon orientation change, ensure that the state of the UI is maintained.
        mTypeSample = (RadioGroup)view.findViewById(R.id.radioGroup1);
        mTypeSample.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioButton:
                        type = 0;
                        break;
                    case R.id.radioButton2:
                        type = 1;
                        break;
                    case R.id.radioButton3:
                        type = 2;
                        break;
                    case R.id.radioButton4:
                        type = 3;
                        break;
                    case R.id.radioButton5:
                        type = 4;
                        break;
                }
            }
        });
        chooseDate = Calendar.getInstance().getTime();
        mDateText = (TextView)view.findViewById(R.id.textViewDate);
        mDateText.setText(timeFormatText.format(chooseDate));
        mButtonRefresh = (Button)view.findViewById(R.id.buttonRefresh);
        mButtonRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chooseDate = Calendar.getInstance().getTime();
                mDateText.setText(timeFormatText.format(chooseDate));
            }
        });
        updateUI();
        return view;
    }
    protected void finishedResolvingNearbyPermissionError() {
        mResolvingNearbyPermissionError = false;
    }
    private void clearDeviceList() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mNearbyDevicesArrayAdapter.clear();
            }
        });
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    @Override
    public void onStart() {
        super.onStart();
        getActivity().getPreferences(Context.MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity().getApplicationContext())
                .addApi(Nearby.MESSAGES_API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
    }
    @Override
    public void onStop() {
        if (mGoogleApiClient.isConnected() && !getActivity().isChangingConfigurations()) {
            // Using Nearby is battery intensive. To preserve battery, stop subscribing or
            // publishing when the fragment is inactive.
            unsubscribe();
            unpublish();
            updateSharedPreference(Constant.KEY_SUBSCRIPTION_TASK, Constant.TASK_NONE);
            updateSharedPreference(Constant.KEY_PUBLICATION_TASK, Constant.TASK_NONE);

            mGoogleApiClient.disconnect();
            getActivity().getPreferences(Context.MODE_PRIVATE)
                    .unregisterOnSharedPreferenceChangeListener(this);
        }
        super.onStop();
    }
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.i(TAG, "GoogleApiClient connected");
        // If the user has requested a subscription or publication task that requires
        // GoogleApiClient to be connected, we keep track of that task and execute it here, since
        // we now have a connected GoogleApiClient.
        executePendingTasks();
    }
    @Override
    public void onConnectionSuspended(int cause) {
        Log.i(TAG, "GoogleApiClient connection suspended: "
                + connectionSuspendedCauseToString(cause));
    }
    private static String connectionSuspendedCauseToString(int cause) {
        switch (cause) {
            case CAUSE_NETWORK_LOST:
                return "CAUSE_NETWORK_LOST";
            case CAUSE_SERVICE_DISCONNECTED:
                return "CAUSE_SERVICE_DISCONNECTED";
            default:
                return "CAUSE_UNKNOWN: " + cause;
        }
    }
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // For simplicity, we don't handle connection failure thoroughly in this sample. Refer to
        // the following Google Play services doc for more details:
        // http://developer.android.com/google/auth/api-client.html
        Log.i(TAG, "connection to GoogleApiClient failed");
    }
    private String getPubSubTask(String taskKey) {
        return getActivity()
                .getPreferences(Context.MODE_PRIVATE)
                .getString(taskKey, Constant.TASK_NONE);
    }
    void executePendingTasks() {
        executePendingSubscriptionTask();
        executePendingPublicationTask();
    }
    void executePendingSubscriptionTask() {
        Log.i(TAG, "12");
        String pendingSubscriptionTask = getPubSubTask(Constant.KEY_SUBSCRIPTION_TASK);
        Log.i(TAG, "13");
        if (TextUtils.equals(pendingSubscriptionTask, Constant.TASK_SUBSCRIBE)) {
            Log.i(TAG, "14");
            subscribe();
        } else if (TextUtils.equals(pendingSubscriptionTask, Constant.TASK_UNSUBSCRIBE)) {
            Log.i(TAG, "15");
            unsubscribe();
        }
    }
    void executePendingPublicationTask() {
        String pendingPublicationTask = getPubSubTask(Constant.KEY_PUBLICATION_TASK);
        if (TextUtils.equals(pendingPublicationTask, Constant.TASK_PUBLISH)) {
            publish();
        } else if (TextUtils.equals(pendingPublicationTask, Constant.TASK_UNPUBLISH)) {
            unpublish();
        }
    }
    private void subscribe() {
        Log.i(TAG, "trying to subscribe");
        Log.i(TAG, "16");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                Log.i(TAG, "17");
                mGoogleApiClient.connect();
                Log.i(TAG, "18");
            }
            Log.i(TAG, "19");
        } else {
            Log.i(TAG, "20");
            clearDeviceList();
            SubscribeOptions options = new SubscribeOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY)
                    .setCallback(new SubscribeCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer subscribing");
                            updateSharedPreference(Constant.KEY_SUBSCRIPTION_TASK,
                                    Constant.TASK_NONE);
                        }
                    }).build();

            Nearby.Messages.subscribe(mGoogleApiClient, mMessageListener, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "subscribed successfully");
                            } else {
                                Log.i(TAG, "could not subscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
    private void unsubscribe() {
        Log.i(TAG, "trying to unsubscribe");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unsubscribe(mGoogleApiClient, mMessageListener)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unsubscribed successfully");
                                updateSharedPreference(Constant.KEY_SUBSCRIPTION_TASK,
                                        Constant.TASK_NONE);
                            } else {
                                Log.i(TAG, "could not unsubscribe");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
    private void publish() {
        SimpleDateFormat timeFormatMessage = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ITALY);
        double valuePressure = 0.0;
        Log.i(TAG, "trying to publish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            PublishOptions options = new PublishOptions.Builder()
                    .setStrategy(PUB_SUB_STRATEGY)
                    .setCallback(new PublishCallback() {
                        @Override
                        public void onExpired() {
                            super.onExpired();
                            Log.i(TAG, "no longer publishing");
                            updateSharedPreference(Constant.KEY_PUBLICATION_TASK,
                                    Constant.TASK_NONE);
                        }
                    }).build();
            try{
                valuePressure = Double.parseDouble(mPressureValue.getText().toString());
            }catch (Exception e){
                Log.d("Error",e.toString());
            }
            mDeviceInfoMessage = pressureMessage.newNearbyMessage(InstanceID.getInstance(getActivity().getApplicationContext()).getId(),
                    type,
                    valuePressure,
                    timeFormatMessage.format(chooseDate));
            Nearby.Messages.publish(mGoogleApiClient, mDeviceInfoMessage, options)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "published successfully");
                            } else {
                                Log.i(TAG, "could not publish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
    private void unpublish() {
        Log.i(TAG, "trying to unpublish");
        // Cannot proceed without a connected GoogleApiClient. Reconnect and execute the pending
        // task in onConnected().
        if (!mGoogleApiClient.isConnected()) {
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        } else {
            Nearby.Messages.unpublish(mGoogleApiClient, mDeviceInfoMessage)
                    .setResultCallback(new ResultCallback<Status>() {

                        @Override
                        public void onResult(Status status) {
                            if (status.isSuccess()) {
                                Log.i(TAG, "unpublished successfully");
                                updateSharedPreference(Constant.KEY_PUBLICATION_TASK,
                                        Constant.TASK_NONE);
                            } else {
                                Log.i(TAG, "could not unpublish");
                                handleUnsuccessfulNearbyResult(status);
                            }
                        }
                    });
        }
    }
    private void handleUnsuccessfulNearbyResult(Status status) {
        Log.i(TAG, "processing error, status = " + status);
        if (status.getStatusCode() == NearbyMessagesStatusCodes.APP_NOT_OPTED_IN) {
            if (!mResolvingNearbyPermissionError) {
                try {
                    mResolvingNearbyPermissionError = true;
                    status.startResolutionForResult(getActivity(),
                            Constant.REQUEST_RESOLVE_ERROR);

                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            if (status.getStatusCode() == ConnectionResult.NETWORK_ERROR) {
                Toast.makeText(getActivity().getApplicationContext(),
                        "No connectivity, cannot proceed. Fix in 'Settings' and try again.",
                        Toast.LENGTH_LONG).show();
                resetToDefaultState();
            } else {
                // To keep things simple, pop a toast for all other error messages.
                Toast.makeText(getActivity().getApplicationContext(), "Unsuccessful: " +
                        status.getStatusMessage(), Toast.LENGTH_LONG).show();
            }

        }
    }
    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, final String key) {
        Log.i(TAG, "8");
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "9");
                if (TextUtils.equals(key, Constant.KEY_SUBSCRIPTION_TASK)) {
                    executePendingSubscriptionTask();
                } else if (TextUtils.equals(key, Constant.KEY_PUBLICATION_TASK)) {
                    executePendingPublicationTask();
                }
                updateUI();
            }
        });
        Log.i(TAG, "10");
    }
    void resetToDefaultState() {
        getActivity().getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putString(Constant.KEY_SUBSCRIPTION_TASK, Constant.TASK_NONE)
                .putString(Constant.KEY_PUBLICATION_TASK, Constant.TASK_NONE)
                .apply();
    }
    private void updateUI() {
        String subscriptionTask = getPubSubTask(Constant.KEY_SUBSCRIPTION_TASK);
        String publicationTask = getPubSubTask(Constant.KEY_PUBLICATION_TASK);

        // Using Nearby is battery intensive. For this reason, progress bars are visible when
        // subscribing or publishing.
        mSubscriptionProgressBar.setVisibility(
                TextUtils.equals(subscriptionTask, Constant.TASK_SUBSCRIBE) ? View.VISIBLE :
                        View.INVISIBLE);
        mPublicationProgressBar.setVisibility(
                TextUtils.equals(publicationTask, Constant.TASK_PUBLISH) ? View.VISIBLE :
                        View.INVISIBLE);

        mSubscriptionImageButton.setImageResource(
                TextUtils.equals(subscriptionTask, Constant.TASK_SUBSCRIBE) ?
                        R.drawable.ic_cancel : R.drawable.ic_nearby);

        mPublicationImageButton.setImageResource(
                TextUtils.equals(publicationTask, Constant.TASK_PUBLISH) ?
                        R.drawable.ic_cancel : R.drawable.ic_share);
    }
    private void updateSharedPreference(String key, String value) {
        Log.i(TAG, "6");
        getActivity().getPreferences(Context.MODE_PRIVATE)
                .edit()
                .putString(key, value)
                .apply();
        Log.i(TAG, "7");
    }
}